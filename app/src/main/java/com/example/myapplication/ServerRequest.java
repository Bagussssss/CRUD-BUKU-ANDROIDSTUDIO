package com.example.myapplication;

import org.apache.http.HttpConnection;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpAbstractParamBean;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ServerRequest {
    final String serverUri = "http://192.168.43.241/req";
    static final String urlSelectAll = "select_all.php";
    static final String urlDelete = "delete_buku.php";
    static final String urlSubmit = "submit_buku.php";

    public ServerRequest(){
        super();
    }
    // mengirim getrequest
    public String sendGetRequest(String reqUrl){
        HttpClient httpClient;
        HttpRequest httGet = new HttpGet(serverUri+"/"+reqUrl);
        InputStream is = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 3000);
            HttpConnectionParams.setSoTimeout(params, 3000);
            httpClient = new DefaultHttpClient(params);
            HttpResponse httpResponse = httpClient.execute((HttpUriRequest) httGet);
            StatusLine status = httpResponse.getStatusLine();

            if (status.getStatusCode() == HttpStatus.SC_OK && httpResponse != null) {
                //mengambil response string dari server
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line+"\n");
                }
                is.close();
            }
        } catch (IOException e) {
            e.getMessage();
        }
        return stringBuilder.toString();
    }

    //mengirim post request
    public int sendPostRequest (Buku buku, String url) {
        int replyCode = 99;
        HttpClient httpClient;
        HttpPost post = new HttpPost(this.serverUri+"/"+url);
        List<NameValuePair> value = new ArrayList<NameValuePair>();
        //menambahkan parameter kedalam request
        value.add(new BasicNameValuePair("id", buku.getId().toString()));
        value.add(new BasicNameValuePair("judul", buku.getJudul()));
        value.add(new BasicNameValuePair("pengarang", buku.getPengarang()));
        value.add(new BasicNameValuePair("deskripsi", buku.getDeskripsi()));
        try {
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 3000);
            HttpConnectionParams.setSoTimeout(params, 3000);
            httpClient = new DefaultHttpClient(params);
            post.setEntity(new UrlEncodedFormEntity(value));
            HttpResponse httpResponse = httpClient.execute(post);
            StatusLine status = httpResponse.getStatusLine();
            if (status.getStatusCode() == HttpStatus.SC_OK) {
                replyCode = status.getStatusCode();
            }
        } catch (IOException e) {
            e.getMessage();
        }
        return replyCode;
    }


}
