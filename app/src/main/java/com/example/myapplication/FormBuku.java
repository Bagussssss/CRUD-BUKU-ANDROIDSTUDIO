package com.example.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import org.apache.http.HttpStatus;

public class FormBuku extends AppCompatActivity {

    EditText editTextJudul;
    EditText editTextPengarang;
    EditText editTextDeskripsi;
    Button btnSave;
    ProgressDialog progresdialog;
    ServerRequest server;
    int replyCode;
    Buku buku;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_buku);
        getSupportActionBar().setTitle("Form Input Buku");

        editTextJudul = (EditText) findViewById(R.id.editTextJudul);
        editTextPengarang = (EditText) findViewById(R.id.editTextPengarang);
        editTextDeskripsi = (EditText) findViewById(R.id.editTextDeskripsi);

        server = new ServerRequest();
        ActionBar actionbar = getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        buku = new Buku();
        if (getIntent().hasExtra("id")) {
            String id = getIntent().getStringExtra("id");
            String judul = getIntent().getStringExtra("judul");
            String pengarang = getIntent().getStringExtra("pengarang");
            String deskripsi = getIntent().getStringExtra("deskripsi");
            editTextJudul.setText(judul);
            editTextPengarang.setText(pengarang);
            editTextDeskripsi.setText(deskripsi);
            buku.setId(Integer.valueOf(id));
        }
        buku.setId(0);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_buku, menu);
        return true;
    }

    private void goToMainActivity() {
        Intent in = new Intent(getApplicationContext(), MainActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                goToMainActivity();
                break;
            case R.id.option_menu_save :
                if (editTextJudul.getText().toString().trim().isEmpty() || editTextPengarang.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Data Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                }
                else {
                    new FormBukuAsync().execute();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendRequest() {
        String judul = editTextJudul.getText().toString();
        String pengarang = editTextPengarang.getText().toString();
        String deskripsi = editTextDeskripsi.getText().toString();
        buku.setJudul(judul);
        buku.setPengarang(pengarang);
        buku.setDeskripsi(deskripsi);
        //mengririm post ke serverrequest
        replyCode = server.sendPostRequest(buku, ServerRequest.urlSubmit);
    }

    private class FormBukuAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            progresdialog = new ProgressDialog(FormBuku.this);
            progresdialog.setMessage("Sedang Menyimpan Data ...");
            progresdialog.setIndeterminate(false);
            progresdialog.setCancelable(false);
            progresdialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            sendRequest();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progresdialog.dismiss();
            if (replyCode == HttpStatus.SC_OK) {
                goToMainActivity();
            }
            else {
                Toast.makeText(getApplicationContext(), "Simpan Data Gagal", Toast.LENGTH_SHORT).show();
            }
        }

    }
}
