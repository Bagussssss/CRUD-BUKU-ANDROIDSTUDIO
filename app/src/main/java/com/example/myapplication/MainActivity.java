package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    ListView listview;
    ActionMode actionmode;
    ActionMode.Callback amCallback;
    ProgressDialog progressdialog;
    ServerRequest serverrequest;
    List<Buku> list;
    ListAdapterBuku adapter;
    Buku selectedList;
    SwipeRefreshLayout SwipeRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serverrequest = new ServerRequest();
        listview = (ListView) findViewById(R.id.listView1);
        SwipeRefresh = findViewById(R.id.refresh);
        SwipeRefresh.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        SwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refreshitem();
                    }
                    void refreshitem() {
                        new MainActivityAsync().execute("load");
                        onItemLoad();
                    }
                    void onItemLoad(){
                        SwipeRefresh.setRefreshing(false);
                    }
                }, 5000);
            }
        });
        amCallback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                getMenuInflater().inflate(R.menu.activity_main_action, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionmode = null;
            }
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_menu_edit:
                        showUpdateForm();
                        break;
                    case R.id.action_menu_delete:
                        delete();
                        break;
                }
                mode.finish();
                return false;
            }


        };
        list = new ArrayList<Buku>();
        // melakukan load data melalui AsyncTask
        new MainActivityAsync().execute("load");

    }


    private void showUpdateForm() {
        Intent in = new Intent(getApplicationContext(), FormUbah.class);
        in.putExtra("id", selectedList.getId().toString());
        in.putExtra("judul", selectedList.getJudul());
        in.putExtra("pengarang", selectedList.getPengarang());
        in.putExtra("deskripsi", selectedList.getDeskripsi());
        startActivity(in);
    }

    private void delete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Delete " + selectedList.getJudul() + " ?");
        builder.setTitle("Delete");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new MainActivityAsync().execute("delete");
                list.remove(list.indexOf(selectedList));
                Toast.makeText(getApplicationContext(), "Detele Success", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_delete);
        alert.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.action_menu_search);
        SearchView searchview = (SearchView)item.getActionView();
        SearchManager searchmanager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchview.setSearchableInfo(searchmanager.getSearchableInfo(getComponentName()));
        searchview.setIconifiedByDefault(false);
        searchview.setOnQueryTextListener(this);
        searchview.setQueryHint("Cari Judul");
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_new :
                Intent in = new Intent(getApplicationContext(), FormBuku.class);
                startActivity(in);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<Buku> processResponse(String response) {
        List<Buku> list = new ArrayList<Buku>();
        try {
            JSONObject jsonObj = new JSONObject(response);
            JSONArray jsonArray = jsonObj.getJSONArray("tb_buku");
            Buku bk = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                bk = new Buku();
                bk.setId(obj.getInt("id"));
                bk.setJudul(obj.getString("judul"));
                bk.setPengarang(obj.getString("pengarang"));
                bk.setDeskripsi(obj.getString("deskripsi"));
                list.add(bk);
            }
        } catch (JSONException e) {
            e.getMessage();
        }
        return list;
    }

    private void populateListView() {
        adapter = new ListAdapterBuku(getApplicationContext(), list);
        listview.setAdapter(adapter);
        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (actionmode != null) {
                    return false;
                }
                actionmode = startActionMode(amCallback);
                view.setSelected(true);
                selectedList = (Buku) adapter.getItem(position);
                return true;
            }
        });

        listview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedList = (Buku) adapter.getItem(position);
                Intent in = new Intent(getApplicationContext(), DetailBuku.class);
                in.putExtra("id", selectedList.getId().toString());
                in.putExtra("judul", selectedList.getJudul());
                in.putExtra("pengarang", selectedList.getPengarang());
                in.putExtra("deskripsi", selectedList.getDeskripsi());
                startActivity(in);
            }
        });


    }

    private class MainActivityAsync extends  AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            progressdialog = new ProgressDialog(MainActivity.this);
            progressdialog.setMessage("sedang proses ...");
            progressdialog.setIndeterminate(false);
            progressdialog.setCancelable(false);
            progressdialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            if (strings[0] == "delete" ) {
                serverrequest.sendGetRequest(ServerRequest.urlDelete + "?id=" + selectedList.getId().toString());
            } else {
                //mengirim request ke server dan memproses JSON response
                String response = serverrequest.sendGetRequest(ServerRequest.urlSelectAll);
                list = processResponse(response);
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            progressdialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    populateListView();
                }
            });
        }
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }
}
