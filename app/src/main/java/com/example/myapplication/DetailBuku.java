package com.example.myapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class DetailBuku extends AppCompatActivity {
    EditText editTextId;
    EditText editTextJudul;
    EditText editTextPengarang;
    EditText editTextDeskripsi;
    Buku buku;
    ServerRequest server;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_buku);
        getSupportActionBar().setTitle("Detail Buku");

        buku = new Buku();
        server = new ServerRequest();
        initView();
        ActionBar actionbar = getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initView() {
        editTextId = (EditText) findViewById(R.id.editTextId);
        editTextJudul = (EditText) findViewById(R.id.editTextJudul);
        editTextPengarang = (EditText) findViewById(R.id.editTextPengarang);
        editTextDeskripsi = (EditText) findViewById(R.id.editTextDeskripsi);

        String id = getIntent().getStringExtra("id");
        String judul = getIntent().getStringExtra("judul");
        String pengarang = getIntent().getStringExtra("pengarang");
        String deskripsi = getIntent().getStringExtra("deskripsi");

        editTextId.setText(id);
        editTextJudul.setText(judul);
        editTextPengarang.setText(pengarang);
        editTextDeskripsi.setText(deskripsi);

        buku.setId(Integer.valueOf(id));
        buku.setJudul(judul);
        buku.setPengarang(pengarang);
        buku.setDeskripsi(deskripsi);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_action, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                goToMainActivity();
                break;
            case R.id.action_menu_edit :
                Intent in = new Intent(getApplicationContext(), FormUbah.class);
                in.putExtra("id", buku.getId().toString());
                in.putExtra("judul", buku.getJudul());
                in.putExtra("pengarang", buku.getPengarang());
                in.putExtra("deskripsi", buku.getDeskripsi());
                startActivity(in);
                break;

            case R.id.action_menu_delete :
                delete();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToMainActivity() {
        Intent in = new Intent(getApplicationContext(), MainActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);
    }
    private void delete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Delete " + buku.getJudul() + " ?");
        builder.setTitle("Delete");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DetailBukuAsync().execute();
                Toast.makeText(getApplicationContext(), "Detele Success", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_delete);
        alert.show();
    }

    private class DetailBukuAsync extends AsyncTask<String, String, String> {
        @Override
        protected void onPostExecute(String s) {
            Intent in = new Intent(getApplicationContext(), MainActivity.class);
            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(in);
        }

        @Override
        protected String doInBackground(String... strings) {
            server.sendGetRequest(ServerRequest.urlDelete+"?id="+buku.getId().toString());
            return null;
        }
    }
}
