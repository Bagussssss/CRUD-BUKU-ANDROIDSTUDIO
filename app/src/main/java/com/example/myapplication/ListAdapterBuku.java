package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ListAdapterBuku extends BaseAdapter implements Filterable {

    Context context;
    List<Buku> list;
    List<Buku> filterd;

    public ListAdapterBuku(Context context, List<Buku> list) {
        this.context = context;
        this.list = list;
        this.filterd = this.list;
    }

    @Override
    public int getCount() {
        return filterd.size();
    }

    @Override
    public Object getItem(int position) {
        return filterd.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(R.layout.konten_list, null);
        }
        Buku bk = filterd.get(position);

        TextView tampil_id = (TextView) convertView.findViewById(R.id.textView2);
        tampil_id.setText(bk.getDeskripsi());

        TextView tampil_judul = (TextView) convertView.findViewById(R.id.textView1);
        tampil_judul.setText(bk.getJudul());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        BukuFilter filter = new BukuFilter();
        return filter;
    }

    private class BukuFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Buku> filteredData = new ArrayList<Buku>();
            FilterResults result = new FilterResults();
            String filterString = constraint.toString().toLowerCase();
            for (Buku bk: list) {
                if (bk.getDeskripsi().toLowerCase().contains(filterString) || bk.getJudul().toLowerCase().contains(filterString)) {
                    filteredData.add(bk);
                }
            }
            result.count = filteredData.size();
            result.values = filteredData;
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filterd = (List<Buku>) results.values;
            notifyDataSetChanged();
        }
    }

}
